clear;
%--------------------------------------------------------------------------
%--------------------------------Parametros--------------------------------
%--------------------------------------------------------------------------
freqAmToForce = 200;
forcePeriod = 1/freqAmToForce;

freqAmToLocation = freqAmToForce/10;
locationPeriod = 1/freqAmToLocation;
%--------------------------------------------------------------------------
%%
%-------------importando os dados do experimento---------------------------
forceSignal = importdata('piloto_eduardo_forceSignal.txt');
alvoAtingido = importdata('piloto_eduardo_triggerAlvoAtingido.txt');
atHome = importdata('piloto_eduardo_triggerAlvoAtHome.txt');
triggerLocation = importdata('piloto_eduardo_triggerLocation.txt');
posicao = importdata('piloto_eduardo_location.txt');
underOverShoot = importdata('piloto_eduardo_resultsAcuracia.txt');
percentErrors = importdata('piloto_eduardo_resultsErrors.txt');
alvoAtingido = alvoAtingido*600;
atHome = atHome*600;

%% 
tempoForce = 1:length(forceSignal);
tempoForce = tempoForce.*forcePeriod;
tempoForce = tempoForce - forcePeriod;


tempoLocation = 1:length(posicao);
tempoLocation = tempoLocation.*locationPeriod;
tempoLocation = tempoLocation - locationPeriod;

%%
%--------------------------------------------------------------------------
%---------------------------Variaveis--------------------------------------
%--------------------------------------------------------------------------
%cell para cada trial
data.force = (cell(length(underOverShoot(:,2)),1));
data.location = (cell(length(underOverShoot(:,2)),1));
data.acuracia = underOverShoot(:,2);
data.percentErrors = percentErrors(:,2);
%--------------------------------------------------------------------------
%% 
%posi��o, em amostras, do come�o e fim de cada trial
TRIAL_FORCEo = find(atHome>=1);
TRIAL_FORCEf = ajustaTrigger( alvoAtingido, underOverShoot);
%Separando cada trial no sinal de for�a
for i=1:length(TRIAL_FORCEo)
   data.force{i} = forceSignal(TRIAL_FORCEo(i):TRIAL_FORCEf(i)); 
end

TRIAL_POSICAOo = find(triggerLocation(:,2) == -1);
TRIAL_POSICAOf = ajustaTrigger(triggerLocation(:,2),underOverShoot);

for i=1:length(TRIAL_POSICAOo)
   data.location{i} = posicao((TRIAL_POSICAOo(i):TRIAL_POSICAOf(i)),2); 
end
%% 
%forceSignal = forceSignal.*(3.3/4096);
figure();
plot(tempoForce,forceSignal,tempoForce,alvoAtingido);
hold on;
title('C�lula de carga parada');
xlabel('Tempo [s]');
ylabel('For�a [V]');
hold off;
% plot(forceSignal);hold on;plot(alvoAtingido);hold on;plot(atHome);

figure();
plot(tempoLocation,posicao(:,2),tempoLocation,((triggerLocation(:,2)*600)+600));
hold on;
title('C�lula de carga parada');
xlabel('Tempo [s]');
ylabel('Pixels');
hold off;
% plot(posicao(:,2));hold on;plot(triggerLocation(:,2));

i=0;