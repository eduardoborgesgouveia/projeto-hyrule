clear;
%--------------------------------------------------------------------------
%--------------------------------Parametros--------------------------------
%--------------------------------------------------------------------------
freqAmToForce = 198;
forcePeriod = 1/freqAmToForce;

freqAmToLocation = freqAmToForce/10;
locationPeriod = 1/freqAmToLocation;
%--------------------------------------------------------------------------
%%
%-------------importando os dados do experimento---------------------------
forceSignal = importdata('02-04_nivelDC_forceSignal.txt');
posicao = importdata('02-04_nivelDC_location.txt');

%% 
tempoForce = 1:length(forceSignal);
tempoForce = tempoForce.*forcePeriod;
tempoForce = tempoForce - forcePeriod;


tempoLocation = 1:length(posicao);
tempoLocation = tempoLocation.*locationPeriod;
tempoLocation = tempoLocation - locationPeriod;

%%
%--------------------------------------------------------------------------
%---------------------------Variaveis--------------------------------------
%--------------------------------------------------------------------------
%cell para cada trial
data.force = (cell(length(forceSignal),1));
data.location = (cell(length(forceSignal),1));
%--------------------------------------------------------------------------
%% 
% %posi��o, em amostras, do come�o e fim de cada trial
% TRIAL_FORCEo = find(atHome>=1);
% TRIAL_FORCEf = ajustaTrigger( alvoAtingido, underOverShoot);
% %Separando cada trial no sinal de for�a
% for i=1:length(TRIAL_FORCEo)
%    data.force{i} = forceSignal(TRIAL_FORCEo(i):TRIAL_FORCEf(i)); 
% end
% 
% TRIAL_POSICAOo = find(triggerLocation(:,2) == -1);
% TRIAL_POSICAOf = ajustaTrigger(triggerLocation(:,2),underOverShoot);
% 
% for i=1:length(TRIAL_POSICAOo)
%    data.location{i} = posicao((TRIAL_POSICAOo(i):TRIAL_POSICAOf(i)),2); 
% end
%% 
forceSignal = forceSignal.*(3.3/4096);
figure();
plot(tempoForce,forceSignal);
hold on;
title('C�lula de carga parada');
xlabel('Tempo [s]');
ylabel('For�a [V]');
hold off;
% plot(forceSignal);hold on;plot(alvoAtingido);hold on;plot(atHome);

figure();
plot(tempoLocation,posicao(:,2));
hold on;
title('C�lula de carga parada');
xlabel('Tempo [s]');
ylabel('Pixels');
hold off;
% plot(posicao(:,2));hold on;plot(triggerLocation(:,2));

i=0;