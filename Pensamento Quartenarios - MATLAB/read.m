clear;
%--------------------------------------------------------------------------
%--------------------------------Parametros--------------------------------
%--------------------------------------------------------------------------
freqAm = 200;
periodo = 1/freqAm;

%--------------------------------------------------------------------------
%%
%-------------importando os dados do experimento---------------------------
forceSignal = importdata('a_a_forceSignal.txt');
alvoAtingido = importdata('a_a_triggerAlvoAtingido.txt');
atHome = importdata('a_a_triggerAlvoAtHome.txt');
triggerLocation = importdata('a_a_triggerLocation.txt');
posicao = importdata('a_a_location.txt');
underOverShoot = importdata('a_a_resultsAcuracia.txt');
percentErrors = importdata('a_a_resultsErrors.txt');
alvoAtingido = alvoAtingido*600;
atHome = atHome*600;

%% teste pra eu saber se consigo calcular o tempo sem pedir ajuda ao T�lio
%rsrs
tempoForce = length(forceSignal) + 20;%%quantidade de amostras + tamanho Janela MM
tempoForce = tempoForce/freqAm;
%%Parece errado - pesquisar


%%
%--------------------------------------------------------------------------
%---------------------------Variaveis--------------------------------------
%--------------------------------------------------------------------------
%cell para cada trial
data.force = (cell(length(underOverShoot(:,2)),1));
data.location = (cell(length(underOverShoot(:,2)),1));
data.acuracia = underOverShoot(:,2);
data.percentErrors = percentErrors(:,2);
%--------------------------------------------------------------------------
%% 
% %posi��o, em amostras, do come�o e fim de cada trial
% TRIAL_FORCEo = find(atHome>=1);
% TRIAL_FORCEf = ajustaTrigger( alvoAtingido, underOverShoot);
% %Separando cada trial no sinal de for�a
% for i=1:length(TRIAL_FORCEo)
%    data.force{i} = forceSignal(TRIAL_FORCEo(i):TRIAL_FORCEf(i)); 
% end
% 
% TRIAL_POSICAOo = find(triggerLocation(:,2) == -1);
% TRIAL_POSICAOf = ajustaTrigger(triggerLocation(:,2),underOverShoot);
% 
% for i=1:length(TRIAL_POSICAOo)
%    data.location{i} = posicao((TRIAL_POSICAOo(i):TRIAL_POSICAOf(i)),2); 
% end
%% 

figure();

plot(forceSignal);hold on;plot(alvoAtingido);hold on;plot(atHome);

figure();
plot(posicao(:,2));hold on;plot(triggerLocation(:,2));

i=0;