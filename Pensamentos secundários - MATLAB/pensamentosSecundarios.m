clear;
%--------------------------------------------------------------------------
freqAm = 200;
periodo = 1/freqAm;
%--------------------------------------------------------------------------
forceSignal = importdata('forceSignal_load2.txt');
alvoAtingido = importdata('triggerAlvoAtingido_load2.txt');
atHome = importdata('triggerAlvoAtHome_load2.txt');
posicao = importdata('location_load2.txt');
alvoAtingido = alvoAtingido*300;
atHome = atHome*300;
tempoForce = 1:periodo:length(forceSignal);
periodoPosicao = tempoForce(end)/length(posicao);
tempoPosicao = 1:periodoPosicao:length(posicao);

figure();
plot(forceSignal,tempoForce);hold on;plot(alvoAtingido,tempoForce);hold on;plot(atHome,tempoForce);

figure();
plot((posicao(:,2)),tempoPosicao);

i=0;