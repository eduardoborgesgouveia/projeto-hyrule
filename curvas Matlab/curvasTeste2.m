%Bits do conversor AD
minimoDivConversor = 3.3/(2^12);

%Entrada de for�a pela c�lula de carga
forcaOrig = 1.028:minimoDivConversor:1.41;

%Re-escalonamento da for�a pra poder variar em um range maior
forcaReEscalada = 0:minimoDivConversor:6;

%curva Exponencial que ditar� o movimento do cursor na tela
curvaExponencial = 0:minimoDivConversor:6;
curvaExponencial = exp(curvaExponencial);

%Tela em pixels
tela = 613:1296;


%m�ximo e min�nmo da entrada de for�a pela c�lula de carga, utilizados na convers�o
maxForcaOrig = forcaOrig(end);
minForcaOrig = forcaOrig(1);

%m�ximo e min�nmo do re-escalonamento, utilizados na convers�o
maxForcaReEscalada = forcaReEscalada(end);
minForcaReEscalada = forcaReEscalada(1);

%Convers�o de valores por y-yo = m(x-xo)
conversao1 = ((forcaOrig - minForcaOrig).* (maxForcaReEscalada - minForcaReEscalada) ./ (maxForcaOrig - minForcaOrig)) + minForcaReEscalada;

%Fazendo a exponencial da curva re-escalonada
exponencialConversao = exp(conversao1);

%M�ximo e m�nimo da curva exponencial que ditar� o movimento do cursor na
%tela
maxCurvaExponencial = curvaExponencial(end);
minCurvaExponencial = curvaExponencial(1);

%M�ximo e m�nimo dos valores da tela, em pixels
maxTela = tela(end);
minTela = tela(1);

%Convers�o de valores por y-yo = m(x-xo)
conversaoFinal = ((exponencialConversao-minCurvaExponencial).*(maxTela-minTela) ./ (maxCurvaExponencial-minCurvaExponencial)) + minTela; 


figure();
subplot(1,3,1);
hold on;
title('Tens�o (Forca) Re-escalada');
hold on;
plot(forcaReEscalada);
subplot(1,3,2);
hold on;
title('Exponencial do re-escalonamento');
hold on;
plot(exponencialConversao);
subplot(1,3,3);
hold on;
title('Resposta na tela');
hold on;
plot(conversaoFinal);
