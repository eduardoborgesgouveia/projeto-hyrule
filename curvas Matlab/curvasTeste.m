minimoDivConversor = 3.3/(2^12);
curva = 0:minimoDivConversor:1;
curvaExp = exp(curva);
tensao = 0.26:minimoDivConversor:0.52;
tela = 613:1296;

maxCurvaExp = curvaExp(end);
minCurvaExp = curvaExp(1);

maxTensao = tensao(end);
minTensao = tensao(1);

maxTela = tela(end);
minTela = tela(1);

expTensao = exp(tensao);
maxExpTensao = expTensao(end);
minExpTensao = expTensao(1);

conversaoForcaExp = ((tensao-minTensao).*(maxCurvaExp-minCurvaExp) ./ (maxTensao-minTensao)) + minCurvaExp; 

minConversaoForcaExp = conversaoForcaExp(1);
maxConversaoForcaExp = conversaoForcaExp(end);

conversaoExpTela = ((conversaoForcaExp-minConversaoForcaExp).*(maxTela-minTela) ./ (maxConversaoForcaExp-minConversaoForcaExp)) + minTela; 

figure();
subplot(1,4,1);
hold on;
title('Tens�o (Forca)');
hold on;
plot(tensao);
subplot(1,4,2);
hold on;
title('Exponencial');
hold on;
plot(curvaExp);
subplot(1,4,3);
hold on;
title('Tensao em Exponencial');
hold on;
plot(conversaoForcaExp);
subplot(1,4,4);
hold on;
title('Tensao em Exponencial em Tela');
hold on;
plot(conversaoExpTela);