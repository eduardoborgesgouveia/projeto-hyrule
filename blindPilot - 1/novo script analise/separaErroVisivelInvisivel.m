function [vetorErrorsVisivel,vetorErrorsInvisivel] = separaErroVisivelInvisivel(erroPixel,centroAlvoAbs,qtdeTrials,flagToAbs)
%Como o protocolo experimental consiste em realizar alguns trials a cegas e
%alguns vendo o cursor se mover na tela, precisamos separar os valores de
%erros em duas bases diferentes para poder comparar

t = 1;
j = 1;
k = 1;
for i=1:qtdeTrials
    if(flag)
        if(t <=7)
            vetorErrorsVisivel(j) =  abs(erroPixel(i) - centroAlvoAbs); 
            j = j+1;
        else if(t<=10)
                vetorErrorsInvisivel(k) = abs(erroPixel(i) - centroAlvoAbs);
                k = k+1;
            end
        end
    else
         if(t <=7)
            vetorErrorsVisivel(j) =  centroAlvoAbs - erroPixel(i); 
            j = j+1;
        else if(t<=10)
                vetorErrorsInvisivel(k) = centroAlvoAbs - erroPixel(i);
                k = k+1;
            end
        end
    end
    t = t+1;
    if(t>10)
        t = 1;
    end
end

end

