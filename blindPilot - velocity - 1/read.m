clear;
%--------------------------------------------------------------------------
%--------------------------------Parametros--------------------------------
%--------------------------------------------------------------------------
freqAmToForce = 200;
forcePeriod = 1/freqAmToForce;

freqAmToLocation = freqAmToForce/10;
locationPeriod = 1/freqAmToLocation;

qtdeBlocos = 7;
qtdeTrials = 70;
qtdeTrialPorBloco = 10;
%--------------------------------------------------------------------------
%%
%-------------importando os dados do experimento---------------------------
forceSignal = importdata('velocity_EduardoBlindPilot_forceSignal.txt');
posicao = importdata('velocity_EduardoBlindPilot_location.txt');
underOverShoot = importdata('velocity_EduardoBlindPilot_resultsAcuracia.txt');
centroAlvoLeft = 1097;
centroAlvoRigth = 1197;

%% 
tempoForce = 1:length(forceSignal);
tempoForce = tempoForce.*forcePeriod;
tempoForce = tempoForce - forcePeriod;


tempoLocation = 1:length(posicao);
tempoLocation = tempoLocation.*locationPeriod;
tempoLocation = tempoLocation - locationPeriod;


%%
%--------------------------------------------------------------------------
%---------------------------Variaveis--------------------------------------
%--------------------------------------------------------------------------
%cell para cada trial
data.force = (cell(length(underOverShoot(:,2)),1));
data.location = (cell(length(underOverShoot(:,2)),1));
data.acuracia = underOverShoot(:,2);
data.forcePorBloco = (cell(7,1));
data.posicaoPorBloco = (cell(7,1));
%--------------------------------------------------------------------------

%% separa��o por bloco
%  POSi = 1;
%  POSf = 10;
% for i = 1:length(data.forcePorBloco)
%    data.forcePorBloco{i} = forceSignal(TRIAL_FORCEo(POSi):TRIAL_FORCEf(POSf));
%    POSi = POSf;
%    POSf = POSi + 10;
% end
% 
% figure();
% for i=1:length(data.forcePorBloco)
%     subplot(7,1,i);
%     plot(data.forcePorBloco{i});
% end
POS_LOCATIONi = 1;
POS_FORCAi = 40;
for i = 1:qtdeBlocos
    POS_LOCATIONf = find(posicao(:,1) == (i-1)*qtdeTrialPorBloco +(qtdeTrialPorBloco-1));
    POS_LOCATIONf = POS_LOCATIONf(end);
    POS_FORCAf = POS_LOCATIONf*10;
    data.posicaoPorBloco{i} = posicao(POS_LOCATIONi:POS_LOCATIONf,2);
    data.forcePorBloco{i} = forceSignal(POS_FORCAi:POS_FORCAf);
    POS_LOCATIONi = POS_LOCATIONf+1;
    POS_FORCAi = POS_LOCATIONi*10;
end

figure();
for i=1:length(data.posicaoPorBloco)  
    subplot(7,1,i);
    x = (((1:length(data.posicaoPorBloco{i})).*locationPeriod)-locationPeriod);
    
    for j = 1:length(x)
       vetorCentroAlvoLeft(j) = centroAlvoLeft;
       vetorCentroAlvoRight(j) = centroAlvoRigth;       
    end
    
    hold on;
    plot(x,vetorCentroAlvoLeft);
    hold on;
    plot(x,vetorCentroAlvoRight);
    hold on;
    plot(x,data.posicaoPorBloco{i});
    hold on;
    bl = strcat('Bloco ', int2str(i));
    ylabel({bl;'Posi��o'});
    ylim([min(data.posicaoPorBloco{i});max(data.posicaoPorBloco{i})+100]);
    
    clear vetorCentroAlvoLeft;
    clear vetorCentroAlvoRight;
end
hold on;
xlabel('Tempo [s]');
hold off;

figure();
for i=1:length(data.forcePorBloco)  
    subplot(7,1,i);
    x = (((1:length(data.forcePorBloco{i})).*forcePeriod)-forcePeriod);
    data.forcePorBloco{i} = data.forcePorBloco{i}.*(3.3/4096);
    plot(x,data.forcePorBloco{i});
    hold on;
    bl = strcat('Bloco ', int2str(i));
    ylabel({bl;'For�a'});
    ylim([min(data.forcePorBloco{i});max(data.forcePorBloco{i})+0.15]);
end
hold on;
xlabel('Tempo [s]');
hold off;
%% 
% forceSignal = forceSignal.*(3.3/4096);
% figure();
% % plot(tempoForce,forceSignal,tempoForce,alvoAtingido);
% plot(tempoForce,forceSignal);
% hold on;
% title('For�a');
% xlabel('Tempo [s]');
% ylabel('For�a [V]');
% hold off;
% % plot(forceSignal);hold on;plot(alvoAtingido);hold on;plot(atHome);
% 
% figure();
% % plot(tempoLocation,posicao(:,2),tempoLocation,((triggerLocation(:,2)*600)+600));
% plot(tempoLocation,posicao(:,2));
% hold on;
% title('Posi��o');
% xlabel('Tempo [s]');
% ylabel('Pixels');
% hold off;
% % plot(posicao(:,2));hold on;plot(triggerLocation(:,2));
% 
% i=0;