function [ TRIALajustado ] = ajustaTrigger( trigger, underOverShoot )
%Durante a execu��o do experimento, overshoots s�o marcados duas vezes,
%tanto o momento de overshoot quanto o momento de trial atingido.
%Deve-se ent�o ignorar o trigger de acerto, quando ocorre o overshoot
TRIALajustado = find(trigger>=1);

%ignorando a repeti��o do trigger 'alvo atingido' (Overshoot)
overshootTrial = find(underOverShoot(:,2) == 1);
for i=1:length(overshootTrial)
   trigger(TRIALajustado(overshootTrial(i))) = 0;
end

TRIALajustado = find(trigger>=1);

end

