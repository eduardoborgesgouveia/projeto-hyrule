function [Xregressao,Yregressao] = regressaoMultipla(funcao,grau);
%Fun��o respons�vel por realizar a regressao multipla de uma fun��o
%qualquer

x = 1:length(funcao);
t = polyfit(x,funcao,grau);
Yregressao = polyval(t,x);
Xregressao = x;


end

