function [ erroPixel...
          ,errosInvisiveis...
          ,errosVisiveis...
          ,mediaErrosInvisiveis...
          ,mediaErrosVisiveis] = carregarArquivos()
%Essa fun��o � respons�vel por gerenciar todos os dados salvos em pastas
%diferentes e devolve-los em forma de matrizes onde cada linha representa
%um sujeito diferente

%retirados inicialmente eram o eber e o jhonatan

%caminho para meus dados
addpath('C:/Users/Samsung/Documents/BitBucketHyrule/Coletas/AllData/erroPixel/');
addpath('C:/Users/Samsung/Documents/BitBucketHyrule/Coletas/AllData/data/');
addpath('C:/Users/Samsung/Documents/BitBucketHyrule/Coletas/AllData/mediaErrorsInvisivel/');
addpath('C:/Users/Samsung/Documents/BitBucketHyrule/Coletas/AllData/mediaErrorsVisivel/');
addpath('C:/Users/Samsung/Documents/BitBucketHyrule/Coletas/AllData/vetorErrorsInvisvel/');
addpath('C:/Users/Samsung/Documents/BitBucketHyrule/Coletas/AllData/vetorErrorsVisivel/');


%{
1 - 
2 - Jhonatan (x)
3 - Matheus (x)
4 - 
5 - David (x)
6 - Eliabe (x)
7 - Igor
8 - Henrique (x)
9 - Eber (x)
10 - Danillo
11 - Ronaldo
12 - Lucas (x)
13 - Victor
14 - Pietro
15 - Jose
16 - Rafael (x)
17 - Brunao (x)
%}


%--erroPixel:::Vari�vel que corresponde a posicao m�xima do cursor na tela
%por trial do sujeito

%{
erroPixel(1,:) = importdata('erroPixel_Matheus.mat');
erroPixel(2,:) = importdata('erroPixel_David.mat');
erroPixel(3,:) = importdata('erroPixel_Eliabe.mat');
erroPixel(4,:) = importdata('erroPixel_Henrique.mat');
erroPixel(5,:) = importdata('erroPixel_Eber.mat');
erroPixel(6,:) = importdata('erroPixels_jhonatan.mat');
erroPixel(7,:) = importdata('erroPixel_Igor.mat');
erroPixel(8,:) = importdata('erroPixel_Danillo.mat');
erroPixel(9,:) = importdata('erroPixel_Ronaldo.mat');
erroPixel(10,:) = importdata('erroPixel_Lucas.mat');
erroPixel(11,:) = importdata('erroPixel_Victor.mat');
erroPixel(12,:) = importdata('erroPixel_Pietro.mat');
erroPixel(13,:) = importdata('erroPixel_Jose.mat');
erroPixel(14,:) = importdata('erroPixel_Rafael.mat');
erroPixel(15,:) = importdata('erroPixel_Brunao.mat');
%}
erroPixel(1,:) = importdata('erroPixels_jhonatan.mat');
erroPixel(2,:) = importdata('erroPixel_Matheus.mat');
erroPixel(3,:) = importdata('erroPixel_David.mat');
erroPixel(4,:) = importdata('erroPixel_Eliabe.mat');
erroPixel(5,:) = importdata('erroPixel_Henrique.mat');
erroPixel(6,:) = importdata('erroPixel_Eber.mat');
erroPixel(7,:) = importdata('erroPixel_Lucas.mat');
erroPixel(8,:) = importdata('erroPixel_Rafael.mat');
erroPixel(9,:) = importdata('erroPixel_Brunao.mat');

%--errorsVisivel:::Vari�vel que corresponde ao erro por trial (dos blocos
%visiveis) do sujeito

%{
errosVisiveis(1,:) = importdata('vetorErrorsVisivel_Matheus.mat');
errosVisiveis(2,:) = importdata('vetorErrorsVisivel_David.mat');
errosVisiveis(3,:) = importdata('vetorErrorsVisivel_Eliabe.mat');
errosVisiveis(4,:) = importdata('vetorErrorsVisivel_Henrique.mat');
errosVisiveis(5,:) = importdata('vetorErrorsVisivel_Eber.mat');
errosVisiveis(6,:) = importdata('vetorErrorsVisivel_jhonatan.mat');
errosVisiveis(7,:) = importdata('vetorErrorsVisivel_Igor.mat');
errosVisiveis(8,:) = importdata('vetorErrorsVisivel_Danillo.mat');
errosVisiveis(9,:) = importdata('vetorErrorsVisivel_Ronaldo.mat');
errosVisiveis(10,:) = importdata('vetorErrorsVisivel_Lucas.mat');
errosVisiveis(11,:) = importdata('vetorErrorsVisivel_Victor.mat');
errosVisiveis(12,:) = importdata('vetorErrorsVisivel_Pietro.mat');
errosVisiveis(13,:) = importdata('vetorErrorsVisivel_Jose.mat');
errosVisiveis(14,:) = importdata('vetorErrorsVisivel_Rafael.mat');
errosVisiveis(15,:) = importdata('vetorErrorsVisivel_Brunao.mat');
%}

errosVisiveis(1,:) = importdata('vetorErrorsVisivel_jhonatan.mat');
errosVisiveis(2,:) = importdata('vetorErrorsVisivel_Matheus.mat');
errosVisiveis(3,:) = importdata('vetorErrorsVisivel_David.mat');
errosVisiveis(4,:) = importdata('vetorErrorsVisivel_Eliabe.mat');
errosVisiveis(5,:) = importdata('vetorErrorsVisivel_Henrique.mat');
errosVisiveis(6,:) = importdata('vetorErrorsVisivel_Eber.mat');
errosVisiveis(7,:) = importdata('vetorErrorsVisivel_Lucas.mat');
errosVisiveis(8,:) = importdata('vetorErrorsVisivel_Rafael.mat');
errosVisiveis(9,:) = importdata('vetorErrorsVisivel_Brunao.mat');


%--errorsInvisivel:::Vari�vel que corresponde ao erro por trial (dos blocos
%invisiveis) do sujeito

%{
errosInvisiveis(1,:) = importdata('vetorErrorsInvisivel_Matheus.mat');
errosInvisiveis(2,:) = importdata('vetorErrorsInvisivel_David.mat');
errosInvisiveis(3,:) = importdata('vetorErrorsInvisivel_Eliabe.mat');
errosInvisiveis(4,:) = importdata('vetorErrorsInvisivel_Henrique.mat');
errosInvisiveis(5,:) = importdata('vetorErrorsInvisivel_Eber.mat');
errosInvisiveis(6,:) = importdata('vetorErrorsInvisivel_jhonatan.mat');
errosInvisiveis(7,:) = importdata('vetorErrorsInvisivel_Igor.mat');
errosInvisiveis(8,:) = importdata('vetorErrorsInvisivel_Danillo.mat');
errosInvisiveis(9,:) = importdata('vetorErrorsInvisivel_Ronaldo.mat');
errosInvisiveis(10,:) = importdata('vetorErrorsInvisivel_Lucas.mat');
errosInvisiveis(11,:) = importdata('vetorErrorsInvisivel_Victor.mat');
errosInvisiveis(12,:) = importdata('vetorErrorsInvisivel_Pietro.mat');
errosInvisiveis(13,:) = importdata('vetorErrorsInvisivel_Jose.mat');
errosInvisiveis(14,:) = importdata('vetorErrorsInvisivel_Rafael.mat');
errosInvisiveis(15,:) = importdata('vetorErrorsInvisivel_Brunao.mat');
%}

errosInvisiveis(1,:) = importdata('vetorErrorsInvisivel_jhonatan.mat');
errosInvisiveis(2,:) = importdata('vetorErrorsInvisivel_Matheus.mat');
errosInvisiveis(3,:) = importdata('vetorErrorsInvisivel_David.mat');
errosInvisiveis(4,:) = importdata('vetorErrorsInvisivel_Eliabe.mat');
errosInvisiveis(5,:) = importdata('vetorErrorsInvisivel_Henrique.mat');
errosInvisiveis(6,:) = importdata('vetorErrorsInvisivel_Eber.mat');
errosInvisiveis(7,:) = importdata('vetorErrorsInvisivel_Lucas.mat');
errosInvisiveis(8,:) = importdata('vetorErrorsInvisivel_Rafael.mat');
errosInvisiveis(9,:) = importdata('vetorErrorsInvisivel_Brunao.mat');

%--mediaErrorsInvisivel:::Vari�vel que corresponde ao erro m�dio por trial
%(dos blocos invisiveis) do sujeito

%{
mediaErrosInvisiveis(1,:) = importdata('mediaErrorsInvisivel_Matheus.mat');
mediaErrosInvisiveis(2,:) = importdata('mediaErrorsInvisivel_David.mat');
mediaErrosInvisiveis(3,:) = importdata('mediaErrorsInvisivel_Eliabe.mat');
mediaErrosInvisiveis(4,:) = importdata('mediaErroInvisivel_Henrique.mat');
mediaErrosInvisiveis(5,:) = importdata('mediaErrorInvisivel_Eber.mat');
mediaErrosInvisiveis(6,:) = importdata('mediaErrorsInvisivel_jhonatan.mat');
mediaErrosInvisiveis(7,:) = importdata('mediaErrorsInvisivel_Igor.mat');
mediaErrosInvisiveis(8,:) = importdata('mediaErrorsInvisivel_Danillo.mat');
mediaErrosInvisiveis(9,:) = importdata('mediaErrorsInvisivel_Ronaldo.mat');
mediaErrosInvisiveis(10,:) = importdata('mediaErrorsInvisivel_Lucas.mat');
mediaErrosInvisiveis(11,:) = importdata('mediaErrorInvisivel_Victor.mat');
mediaErrosInvisiveis(12,:) = importdata('mediaErrorsInvisivel_Pietro.mat');
mediaErrosInvisiveis(13,:) = importdata('mediaErrorsInvisivel_Jose.mat');
mediaErrosInvisiveis(14,:) = importdata('mediaErrorsInvisivel_Rafael.mat');
mediaErrosInvisiveis(15,:) = importdata('mediaErrorsInvisivel_Brunao.mat');
%}

mediaErrosInvisiveis(1,:) = importdata('mediaErrorsInvisivel_jhonatan.mat');
mediaErrosInvisiveis(2,:) = importdata('mediaErrorsInvisivel_Matheus.mat');
mediaErrosInvisiveis(3,:) = importdata('mediaErrorsInvisivel_David.mat');
mediaErrosInvisiveis(4,:) = importdata('mediaErrorsInvisivel_Eliabe.mat');
mediaErrosInvisiveis(5,:) = importdata('mediaErroInvisivel_Henrique.mat');
mediaErrosInvisiveis(6,:) = importdata('mediaErrorInvisivel_Eber.mat');
mediaErrosInvisiveis(7,:) = importdata('mediaErrorsInvisivel_Lucas.mat');
mediaErrosInvisiveis(8,:) = importdata('mediaErrorsInvisivel_Rafael.mat');
mediaErrosInvisiveis(9,:) = importdata('mediaErrorsInvisivel_Brunao.mat');

%--mediaErrorsVisivel:::Vari�vel que corresponde ao erro m�dio por trial
%(dos blocos visiveis) do sujeito
%{
mediaErrosVisiveis(1,:) = importdata('mediaErrorsVisivel_Matheus.mat');
mediaErrosVisiveis(2,:) = importdata('mediaErrorsVisivel_David.mat');
mediaErrosVisiveis(3,:) = importdata('mediaErrorsVisivel_Eliabe.mat');
mediaErrosVisiveis(4,:) = importdata('mediaErroVisivel_Henrique.mat');
mediaErrosVisiveis(5,:) = importdata('mediaErrorVisivel_Eber.mat');
mediaErrosVisiveis(6,:) = importdata('mediaErrorsVisivel_jhonatan.mat');
mediaErrosVisiveis(7,:) = importdata('mediaErrorsVisivel_Igor.mat');
mediaErrosVisiveis(8,:) = importdata('mediaErrorsVisivel_Danillo.mat');
mediaErrosVisiveis(9,:) = importdata('mediaErrorsVisivel_Ronaldo.mat');
mediaErrosVisiveis(10,:) = importdata('mediaErrorsVisivel_Lucas.mat');
mediaErrosVisiveis(11,:) = importdata('mediaErrorVisivel_Victor.mat');
mediaErrosVisiveis(12,:) = importdata('mediaErrorsVisivel_Pietro.mat');
mediaErrosVisiveis(13,:) = importdata('mediaErrorsVisivel_Jose.mat');
mediaErrosVisiveis(14,:) = importdata('mediaErrorsVisivel_Rafael.mat');
mediaErrosVisiveis(15,:) = importdata('mediaErrorsVisivel_Brunao.mat');
%}
mediaErrosVisiveis(1,:) = importdata('mediaErrorsVisivel_jhonatan.mat');
mediaErrosVisiveis(2,:) = importdata('mediaErrorsVisivel_Matheus.mat');
mediaErrosVisiveis(3,:) = importdata('mediaErrorsVisivel_David.mat');
mediaErrosVisiveis(4,:) = importdata('mediaErrorsVisivel_Eliabe.mat');
mediaErrosVisiveis(5,:) = importdata('mediaErroVisivel_Henrique.mat');
mediaErrosVisiveis(6,:) = importdata('mediaErrorVisivel_Eber.mat');
mediaErrosVisiveis(7,:) = importdata('mediaErrorsVisivel_Lucas.mat');
mediaErrosVisiveis(8,:) = importdata('mediaErrorsVisivel_Rafael.mat');
mediaErrosVisiveis(9,:) = importdata('mediaErrorsVisivel_Brunao.mat');


end

