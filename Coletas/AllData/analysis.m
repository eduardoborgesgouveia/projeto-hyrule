%{
script para an�lise geral dos resultados de todos os sujeitos que
realizaram as coletas 
%}
clear
%% parametros b�sicos
%O protocolo experimental consiste na realiza��o de 7 blocos contendo 10
%trials cada.
qtdeTrialPorBlocosVisiveis = 5;
qtdeTrialPorBlocosInvisiveis = 5;
qtdeTrials = 70;
qtdeTrialPorBloco = 10;
qtdeBlocosTotal = qtdeTrials/qtdeTrialPorBloco;

%% carregando os arquivos de dados das pessoas e guardando em uma vari�vel
%estou guardando em uma matriz onde cada linha corresponder� a um sujeito
%{
---erroPixel:::Vari�vel que corresponde a posicao m�xima do cursor na tela
por trial do sujeito

---errorsVisivel:::Vari�vel que corresponde ao erro por trial (dos blocos
visiveis) do sujeito

---errorsInvisivel:::Vari�vel que corresponde ao erro por trial (dos blocos
invisiveis) do sujeito

---mediaErrorsInvisivel:::Vari�vel que corresponde ao erro m�dio por trial
(dos blocos invisiveis) do sujeito

---mediaErrorsVisivel:::Vari�vel que corresponde ao erro m�dio por trial
(dos blocos visiveis) do sujeito
%}

[erroPixel...
,errosInvisiveis...
,errosVisiveis...
,mediaErrosInvisiveis...
,mediaErrosVisiveis] = carregarArquivos();

%% fazendo a m�dia geral de cada trial de todos os sujeitos

erroVisivelGeral = mean(errosVisiveis);
erroInvisivelGeral = mean(errosInvisiveis);
x = 1:length(erroPixel(1,:));

j = 1;
POSi=1;
POSf=5;
VISi = 1;
VISf = qtdeTrialPorBlocosVisiveis;
INVi = qtdeTrialPorBlocosVisiveis + 1;
INVf = qtdeTrialPorBlocosVisiveis + qtdeTrialPorBlocosInvisiveis;

figure();
while(INVf<=qtdeTrials)
%     %calulando a regressao multipla dos valores de todos os trials pra
%     %colocar em cinza
%     for q = 1:length(errosVisiveis(:,1))
%         [xRegressao,yRegressao] = regressaoMultipla(errosVisiveis(q,POSi:POSf),2);
%         p3 = plot(x(VISi:VISf),yRegressao,'color',[0 0 0]+0.8);
%         hold on;
%         [xRegressao,yRegressao] = regressaoMultipla(errosInvisiveis(q,POSi:POSf),2);
%         plot(x(INVi:INVf),yRegressao,'color',[0 0 0]+0.8);
%         hold on;
%     end
    
    %fazendo o scatter dos valores para os trials invisiveis e visiveis
    s1 = scatter(x(VISi:VISf),erroVisivelGeral(1,POSi:POSf),'filed','b');
    hold on;
    s2 = scatter(x(INVi:INVf),erroInvisivelGeral(1,POSi:POSf),'filed','r');
    
    %calculando a regress�o multipla dos erros visiveis
    hold on;
    [xRegressao,yRegressao] = regressaoMultipla(erroVisivelGeral(1,POSi:POSf),2);
    p1 = plot(x(VISi:VISf),yRegressao,'k');
    set(p1,'linewidth',1);
    hold on;
    %calculando o desvio padr�o para cada ponto dos visiveis
    stdPointVisivel = std(errosVisiveis(:,POSi:POSf));
    errorbar(x(VISi:VISf),erroVisivelGeral(1,POSi:POSf),stdPointVisivel,'.k');
    
    hold on;
    %calculando o desvio padr�o para cada ponto dos invisiveis
    stdPointInvisivel = std(errosInvisiveis(:,POSi:POSf));
    errorbar(x(INVi:INVf),erroInvisivelGeral(1,POSi:POSf),stdPointInvisivel,'.k');
    
    %calculando a regress�o multipla dos erros invisiveis
    hold on;
    [xRegressao,yRegressao] = regressaoMultipla(erroInvisivelGeral(1,POSi:POSf),2);
    p2 = plot(x(INVi:INVf),yRegressao,'k');
    set(p2,'linewidth',1);
    %Juntando todos os dados em um vetor s�
    erroVisInvisGeral(VISi:VISf) = erroVisivelGeral(1,POSi:POSf);
    erroVisInvisGeral(INVi:INVf) = erroInvisivelGeral(1,POSi:POSf);
    
    %calculando o desvp
    desvPadrao(:,j) = std(erroVisivelGeral(1,POSi:POSf));
    j = j+1;
    desvPadrao(:,j) = std(erroInvisivelGeral(1,POSi:POSf));
    j = j+1;
    
    %ajustando os indices para gerenciamento dos valores de posi��o
    POSi = POSf+1;
    POSf = POSi+qtdeTrialPorBlocosVisiveis-1;
    
    VISi = VISi + qtdeTrialPorBloco;
    VISf = VISf + qtdeTrialPorBloco;
    
    INVi = INVi + qtdeTrialPorBloco;
    INVf = INVf + qtdeTrialPorBloco;
end
title('Mean error per trial');
ylabel('Error in pixels');
xlabel('Trials');
set(gca,'fontsize',13,'fontWeight','bold');
ylim([-1500;1300]);
xlim([-1;71]);
hold on;
legend([s1 s2],{'mean error of the trials with visual feedback','mean error of the trials without visual feedback'});

%% Fazendo a m�dia de cada bloco de cada sujeito

erroBlocoVisivelGeral = mean(mediaErrosVisiveis);
erroBlocoInvisivelGeral = mean(mediaErrosInvisiveis);

j=1;
k=1;
x = linspace(1,2*qtdeBlocosTotal,14);
figure();
for i=1:2*qtdeBlocosTotal
    if(mod(i,2)==1)
        erroMedioVisInvisGeral(i) = erroBlocoVisivelGeral(k);
        p1 = scatter(x(i),erroMedioVisInvisGeral(i), 'filled','b');
        hold on;
        k = k+1;
    else
        erroMedioVisInvisGeral(i) = erroBlocoInvisivelGeral(k-1);
        p2 = scatter(x(i),erroMedioVisInvisGeral(i), 'filled','r');
        hold on;
    end
    j = j+1;
end
matrizErroBloco(1,:) = erroBlocoVisivelGeral(1,:);
matrizErroBloco(2,:) = erroBlocoInvisivelGeral(1,:);
hold on;
errorbar(x,erroMedioVisInvisGeral,desvPadrao,'.k');
xVis = [1 3 5 7 9 11 13];
xInv = [2 4 6 8 10 12 14];
[xRegressaoBlocoVisivel,yRegressaoBlocoVisivel] = regressaoMultiplaManipulavel(xVis,matrizErroBloco(1,:),4);
hold on;
plot(xRegressaoBlocoVisivel,yRegressaoBlocoVisivel);
[xRegressaoBlocoInvisivel,yRegressaoBlocoInvisivel] = regressaoMultiplaManipulavel(xInv,matrizErroBloco(2,:),4);
hold on;
plot(xRegressaoBlocoInvisivel,yRegressaoBlocoInvisivel);
title('Mean error per task');
ylabel('Error in pixels');
xlabel('Sequence of tasks');
set(gca,'fontsize',13,'fontWeight','bold');
ylim([-1500;200]);
legend([p1 p2],{'mean error of the trials with visual feedback','mean error of the trials without visual feedback'});

set(gca,'xtick', 0:1:15);
%C�lculo da correla��o entre as duas curvas
matrizRegressaoErroBloco(1,:) = yRegressaoBlocoVisivel(1,:);
matrizRegressaoErroBloco(2,:) = yRegressaoBlocoInvisivel(1,:);
correlacao = corrcoef(matrizRegressaoErroBloco');


%%
% %% plot dos valores m�dios dos erros por bloco
% matrizErroBloco(1,:) = erroBlocoVisivelGeral(1,:);
% matrizErroBloco(2,:) = erroBlocoInvisivelGeral(1,:);
% 
% xVis = [1 3 5 7 9 11 13];
% xInv = [2 4 6 8 10 12 14];
% [xRegressaoBlocoVisivel,yRegressaoBlocoVisivel] = regressaoMultiplaManipulavel(xVis,matrizErroBloco(1,:),4);
% [xRegressaoBlocoInvisivel,yRegressaoBlocoInvisivel] = regressaoMultiplaManipulavel(xInv,matrizErroBloco(2,:),4);
% 
% matrizRegressaoErroBloco(1,:) = yRegressaoBlocoVisivel(1,:);
% matrizRegressaoErroBloco(2,:) = yRegressaoBlocoInvisivel(1,:);
% figure();
% hold on;
% scatter(x,erroBlocoVisivelGeral,'filled','b');
% hold on;
% scatter(x,erroBlocoInvisivelGeral, 'filled','r');
% hold on;
% plot(xRegressaoBlocoVisivel,yRegressaoBlocoVisivel,'b');
% hold on;
% plot(xRegressaoBlocoVisivel,yRegressaoBlocoInvisivel,'k');
% xlim([0;8]);
% ylim([-700;200]);
% correlacao = corrcoef(matrizRegressaoErroBloco');

%%

figure();
j = 1;
POSi = 1;
POSf = 2;
for i = 1:qtdeBlocosTotal
    erroBar(j,:) = erroMedioVisInvisGeral(POSi:POSf); 
    POSi = POSf + 1;
    POSf = POSi + 1;
    j = j+1;
end
erroBar = abs(erroBar);
bar(erroBar);
title('Mean error per block');
ylabel('Error in pixels');
xlabel('Trials');
legend('absolute mean value error visible per block','absolute mean value error invisible per block');
%ylim([0;300]);
set(gca,'fontsize',13,'fontWeight','bold');

