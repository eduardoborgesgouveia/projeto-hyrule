function [ mediaErroBloco ] = mediaErro(vetorErrors,qtdeBlocos)
%Calculando a m�dia dos valores de erro dos trials, por bloco

    INDEXi = 1;
    INDEXf = INDEXi + qtdeBlocos -1;
    i = 1;

    while(INDEXf<=length(vetorErrors))

        mediaErroBloco(i) = mean(vetorErrors(INDEXi:INDEXf));
        INDEXi = INDEXf+1;
        INDEXf = INDEXf + qtdeBlocos;
        i = i+1;
    end
end

