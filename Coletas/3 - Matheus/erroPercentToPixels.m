function [ erroPixel ] = erroPercentToPixels( erroPercent, leftLim, rightLim )
%{
Os valores de percentErrors s�o dados em porcentagem da tela, para
conseguir os valores em posi��o na tela precisamos converte-los para
pixels, multiplicando eles pelo range de pixels da tela.

Para tanto devemos fazer =

erroEmPixels = limiteEsquerdoDaTela + (limiteDireitoDaTela - limiteEsquerdoDaTela)*erroEmPorcentagem
%}

erroPixel = leftLim + ((rightLim - leftLim).*erroPercent);



end

