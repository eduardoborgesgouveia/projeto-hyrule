clear;
%--------------------------------------------------------------------------
%--------------------------------Parametros--------------------------------
%--------------------------------------------------------------------------
%Frequencia de amostragem do sinal vindo da c�lula de carga
freqAmToForce = 200;
forcePeriod = 1/freqAmToForce;

%Frequencia de amostragem dos valores, de varia��o do cursor na tela, que 
%s�o submetidos a uma esp�cie de m�dia m�vel que gera um valor a cada 10. 
freqAmToLocation = freqAmToForce/10;
locationPeriod = 1/freqAmToLocation;

%O protocolo experimental consiste na realiza��o de 7 blocos contendo 10
%trials cada.
qtdeTrialPorBlocosVisiveis = 7;
qtdeTrialPorBlocosInvisiveis = 3;
qtdeTrials = 70;
qtdeTrialPorBloco = 10;
qtdeBlocosTotal = qtdeTrials/qtdeTrialPorBloco;

%vetor de legenda para plotar gr�ficos de acordo com os blocos
vetorLegendBloco = ['Bloco 1';'Bloco 2';'Bloco 3';'Bloco 4';'Bloco 5';'Bloco 6';'Bloco 7'];
vetorCorBloco = ['y','m','c','r','g','b','k'];

%A tela no experimento pode variar de computador para computador. Por�m
%como o experimento foi feito em um Samsung 14" - modelo: NP300E4M - KW3BR,
%a posi��o do cursor em repouso (leftLim) e do final da tela (ambos em 
%pixels) s�o determinados a seguir
leftLim = 90;
rightLim = 1296;
centroAlvoLeft = 1097;
centroAlvoRigth = 1197;
centroAlvoAbs = centroAlvoLeft + (centroAlvoRigth - centroAlvoLeft);
%--------------------------------------------------------------------------
%%
%--------------------------------------------------------------------------
%-------------importando os dados do experimento---------------------------
%--------------------------------------------------------------------------
forceSignal = importdata('Lopes_Eduardo_forceSignal.txt');
posicao = importdata('Lopes_Eduardo_location.txt');
percentErrors = importdata('Lopes_Eduardo_resultsErrors.txt');


%% Cria��o dos vetores que representam o tempo de execu��o dos blocos
tempoForce = 1:length(forceSignal);
tempoForce = tempoForce.*forcePeriod;
tempoForce = tempoForce - forcePeriod;

%Como a frequencia de amostragem da posi��o do cursor na tela � diferente
%da frequencia de amostragem do valor da c�lula de carga � necessario dois
%vetores de tempo diferentes 
tempoLocation = 1:length(posicao);
tempoLocation = tempoLocation.*locationPeriod;
tempoLocation = tempoLocation - locationPeriod;

%%
%--------------------------------------------------------------------------
%---------------------------Variaveis--------------------------------------
%--------------------------------------------------------------------------
%Agrupando os dados em uma matriz de dados
%variavel para armazenar a for�a
data.forca = (cell(qtdeTrials,1));
data.location = (cell(qtdeTrials,1));
data.percentErrors = percentErrors(:,2);
data.forcePorBloco = (cell(qtdeBlocosTotal,1));
data.posicaoPorBloco = (cell(qtdeBlocosTotal,1));
%--------------------------------------------------------------------------
%% separa��o dos valores de for�a e posi��o por bloco
POS_LOCATIONi = 1;
POS_FORCAi = 40;
for i = 1:qtdeBlocosTotal
    POS_LOCATIONf = find(posicao(:,1) == (i-1)*qtdeTrialPorBloco +(qtdeTrialPorBloco-1));
    POS_LOCATIONf = POS_LOCATIONf(end);
    POS_FORCAf = POS_LOCATIONf*10;
    data.posicaoPorBloco{i} = posicao(POS_LOCATIONi:POS_LOCATIONf,2);
    data.forcePorBloco{i} = forceSignal(POS_FORCAi:POS_FORCAf);
    POS_LOCATIONi = POS_LOCATIONf+1;
    POS_FORCAi = POS_LOCATIONi*10;
end
%%
%%Plotagem dos valores de posi��o do cursor na tela, por bloco
figure();
for i=1:length(data.posicaoPorBloco)  
    subplot(qtdeBlocosTotal,1,i);
    x = (((1:length(data.posicaoPorBloco{i})).*locationPeriod)-locationPeriod);
    plot(x,data.posicaoPorBloco{i});
    if(i==1)
        hold on;title('Cursor location on screen per block');
    end
    hold on;
    bl = strcat('Bloc ', int2str(i));
    ylabel({bl;'Location'});
    ylim([min(data.posicaoPorBloco{i});max(data.posicaoPorBloco{i})+100]);
end
hold on;
xlabel('Time in s');
hold off;


%%Plotagem dos valores de for�a, por bloco
figure();
for i=1:length(data.forcePorBloco)  
    subplot(7,1,i);
    x = (((1:length(data.forcePorBloco{i})).*forcePeriod)-forcePeriod);
    data.forcePorBloco{i} = data.forcePorBloco{i}.*(3.3/4096);
    plot(x,data.forcePorBloco{i});
    if(i==1)
        hold on;title('Strength values per block');
    end
    hold on;
    bl = strcat('Bloc ', int2str(i));
    ylabel({bl;'Strength'});
    ylim([min(data.forcePorBloco{i});max(data.forcePorBloco{i})+0.15]);
end
hold on;
xlabel('Time in s');
hold off;

%% Separando os erros por trials

%Os valores de percentErrors s�o dados em porcentagem da tela, para
%conseguir os valores em posi��o na tela precisamos converte-los para
%pixels, multiplicando eles pelo range de pixels da tela.
erroPixel = erroPercentToPixels(percentErrors(:,2),leftLim,rightLim);

%Como o protocolo experimental consiste em realizar alguns trials a cegas e
%alguns vendo o cursor se mover na tela, precisamos separar os valores de
%erros em duas bases diferentes para poder comparar
[vetorErrorsVisivel,vetorErrorsInvisivel] = separaErroVisivelInvisivel(erroPixel,centroAlvoAbs,qtdeTrials,'false');

%Criando o vetor x para os valores dos erros
xScatterInvisivel = 1:length(vetorErrorsInvisivel);
xScatterVisivelContinuo = 1:length(vetorErrorsVisivel)+1;

%Calculando a m�dia dos valores de erro dos trials visiveis, por bloco
mediaErroVisivelBloco = mediaErro(vetorErrorsVisivel,qtdeTrialPorBlocosVisiveis);

%Calculando a m�dia dos valores de erro dos trials invisiveis, por bloco
mediaErroInvisivelBloco = mediaErro(vetorErrorsInvisivel,qtdeTrialPorBlocosInvisiveis);

%%
%Fazendo regress�o multipla para ver a tendencia do erro dos trials
%visiveis
[xRegressaoVisivel,regressaoErrosVisiveis] = regressaoMultipla(mediaErroVisivelBloco,qtdeBlocosTotal,3);

%Plotando o valor do erro m�dio por bloco
figure();
scatter(xRegressaoVisivel,mediaErroVisivelBloco,'filled');
hold on;
plot(xRegressaoVisivel,regressaoErrosVisiveis,'k');
title('Mean error of visible trials per block');
xlabel('Bloc');
ylabel('Error in pixels');
%ylim([10;150]);
ylim([-rightLim;rightLim]);
set(gca,'fontsize',13,'fontWeight','bold');
xlim([0;qtdeBlocosTotal+1]);

%%
%Fazendo regress�o multipla para ver a tendencia do erro dos trials
%Invisiveis
[xRegressaoInvisivel,regressaoErrosInvisivel] = regressaoMultipla(mediaErroInvisivelBloco,qtdeBlocosTotal,3);

%Plotando o valor do erro m�dio por bloco
figure();
scatter(xRegressaoInvisivel,mediaErroInvisivelBloco,'filled');
hold on;
plot(xRegressaoInvisivel,regressaoErrosInvisivel,'k');
title('Mean error of invisible trials per block');
xlabel('Bloc');
ylabel('Error in pixels');
ylim([-rightLim;rightLim]);
set(gca,'fontsize',13,'fontWeight','bold');
xlim([0;qtdeBlocosTotal+1]);

%% Fazendo a regress�o linear dos valores de erro visiveis (sem m�dia)
 
data.erroVisivelPorBloco = separaErroBloco(vetorErrorsVisivel,qtdeTrialPorBlocosVisiveis);

yVetorRegressaoContinuoErroVisivel = 0;
xVetorRegressaoContinuoErroVisivel = 0;
for i=1:length(data.erroVisivelPorBloco)
   [xRegressaoVisivel,yRegressaoVisivel{i}] = regressaoMultipla(data.erroVisivelPorBloco{i},qtdeTrialPorBlocosVisiveis,2);
end
xVetorRegressaoContinuoErroVisivel = 1:length(yRegressaoVisivel)*qtdeTrialPorBlocosVisiveis;

figure();
j = 1;
for i=1:length(yRegressaoVisivel)
    plot(xVetorRegressaoContinuoErroVisivel(j:(i*length(yRegressaoVisivel{i}))),yRegressaoVisivel{i},'k');
    hold on;
    scatter(xVetorRegressaoContinuoErroVisivel(j:(i*length(yRegressaoVisivel{i}))),data.erroVisivelPorBloco{i},'filled',vetorCorBloco(i));
    if(i==1)
        hold on;title('Error (visible)');
    end
    j = j + length(yRegressaoVisivel{i});
end
xlabel('Visible trial');
ylabel('Error in pixels');
set(gca,'fontsize',13,'fontWeight','bold');
ylim([-rightLim;rightLim]);
%% Fazendo a regress�o linear dos valores de erro invisiveis (sem m�dia)
 
data.erroInvisivelPorBloco = separaErroBloco(vetorErrorsInvisivel,qtdeTrialPorBlocosInvisiveis);

yVetorRegressaoContinuoErroInvisivel = 0;
xVetorRegressaoContinuoErroInvisivel = 0;

for i=1:length(data.erroVisivelPorBloco)
   [xRegressaoInvisivel,yRegressaoInvisivel{i}] = regressaoMultipla(data.erroInvisivelPorBloco{i},qtdeTrialPorBlocosInvisiveis,2);
end
xVetorRegressaoContinuoErroInvisivel = 1:length(yRegressaoInvisivel)*qtdeTrialPorBlocosInvisiveis;

figure();
j = 1;
for i=1:length(yRegressaoInvisivel)
    plot(xVetorRegressaoContinuoErroInvisivel(j:(i*length(yRegressaoInvisivel{i}))),yRegressaoInvisivel{i},'k');
    hold on;
    scatter(xVetorRegressaoContinuoErroInvisivel(j:(i*length(yRegressaoInvisivel{i}))),data.erroInvisivelPorBloco{i},'filled',vetorCorBloco(i));
    if(i==1)
        hold on;title('Error (invisible)');
    end
    j = j + length(yRegressaoInvisivel{i});
end
xlabel('invisible trial');
ylabel('Error in pixels');
set(gca,'fontsize',13,'fontWeight','bold');
ylim([-rightLim;rightLim]);

%% Concatena��o dos trials vis�veis e invis�veis para plotagem 

xVetorErroContinuo = 1:qtdeTrials;

VISi = 1;
VISf = qtdeTrialPorBlocosVisiveis;
INVi = qtdeTrialPorBlocosVisiveis + 1;
INVf = qtdeTrialPorBlocosVisiveis + qtdeTrialPorBlocosInvisiveis;
figure();
for i=1:qtdeBlocosTotal
    plot(xVetorErroContinuo(VISi:VISf),yRegressaoVisivel{i},'k');
    hold on;
    scatter(xVetorErroContinuo(VISi:VISf),data.erroVisivelPorBloco{i},'filled',vetorCorBloco(i));
    hold on;
    plot(xVetorErroContinuo(INVi:INVf),yRegressaoInvisivel{i},'k');
    hold on;
    scatter(xVetorErroContinuo(INVi:INVf),data.erroInvisivelPorBloco{i},'filled',vetorCorBloco(i));
    VISi = VISi + qtdeTrialPorBloco;
    VISf = VISf + qtdeTrialPorBloco;
    INVi = INVi + qtdeTrialPorBloco;
    INVf = INVf + qtdeTrialPorBloco;
end
hold on;
title('Error per trial');
xlabel('Trial');
ylabel('Error in pixels');
set(gca,'fontsize',13,'fontWeight','bold');
xlim([0;qtdeTrials+1]);
ylim([-rightLim;rightLim]);















