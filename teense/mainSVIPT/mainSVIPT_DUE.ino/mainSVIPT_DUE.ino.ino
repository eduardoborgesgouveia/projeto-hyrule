

/*
  Firmware usado no experimento SVIPT.

  1 - Inicializa ports IO, port serial, timer e estado da máquina de estados.
  2 - LOOP
      2.1 - Estado Firmware:
        Estabelece handshake com applicativo via USB:
        2.1.1 - Aguarda chegada de comando "FWR"
        2.1.2 - Envia versão deste Firmware: "Teensy SVIPT 1.0" 
      2.2 - Estado aguarda comandos:
        2.2.1 - Aguarda chegada de comando e inicia sua execução.

*/

//#include <TimerOne.h>
 #include<stdlib.h>

#include <DueTimer.h>


// Pin 13 has an LED connected on most Arduino boards.
// Pin 11 has the LED on Teensy 2.0
// Pin 6  has the LED on Teensy++ 2.0
// Pin 13 has the LED on Teensy 3.0
// give it a name:
int led = 13;

#define baudRate  115200    //Baud rate usada para comunicaçao com host

#define AD_Range    3.3   //Range (fundo de escala analógico) do AD deste Teensy.
#define AD_nBits    12    //Quantidade de bits do AD
#define AD_TxAmost  200   //Os sinais analógicos (da célula de carga serão lidos nesta frequência - HERTZ).
                          //Os valores digitalizados serao enviados em 2 BYTES para aumentar velocidade de
                          //transmissão. Para isso o host precisa saber: AD_Range e AD_nBits.
                          
#define firmware "Teensy SVIPT 1.0" //Versão deste Firmware.
#define terminatorChar 0x00  //terminador de comunicação via STRINGS (null char '\0' = 0x00 - 00 dec)
#define tCMDSize 4           //os nomes do comandos terão sempre esta quantidade de caracteres.

//TODA comunicação no sentido Teensy -> Host (resposta a comandos) será precedida pelo comando
//que a originou, no seguinte formato: | Comand | Data | terminatorChar |


#define tCMD_SendFmw  "FMWR"  //Comando: Host solicita o envio do firmware
                              //Em resposta, Teensy envia o nome do comando seguido do firmware
                              //    "FMWRTeensy SVIPT 1.0'\0'"
#define tCMD_SendADrange  "ADRA"  //Comando: Host solicita o envio do range do conversor AD
                                  //Em resposta, Teensy envia o nome do comando seguido do range em
                                  //um string (range será enviado com uma casa decimal --- 5.0, 3.3 ...)
                                  //    "ADRA5.0'\0'"                                                      
#define tCMD_SendADnbits  "ADNB" //Comando: Host solicita o envio da quantidade de bits do conversor AD.
                                  //Em resposta, Teensy envia o nome do comando seguido do nro de bits em
                                  //um string (nbits - integer)
                                  //    "ADNB10'\0'"
#define tCMD_SendADtxAmost "ADTX" //Comando: Host solicita o envio da taxa de amostragem usada na digitalização
                                  //Em resposta, Teensy envia o nome do comando seguido da tx amostr em
                                  //um string (TxAmostr - integer)
                                  //    "ADNB200'\0'"
#define tCMD_StartAquis  "STAQ" //Comando: Iniciar aquisição de dados e envio pela serial para o host.
                                //Host inicia aquisição de dados e envio para o host.
                                //Os dados serão enviados em pacotes com múltiplos de 2 bytes
                                //(ver abaixo).
                                //Cada pacote será o seguinte formato:
                                // | "STAQ" | #bytesPack | bytes pack |
                                // tCMDSize (atualmente 4) chars com nome do comando, seguido
                                // de 1 BYTE com a quantidade de bytes do pacote, seguido dos bytes do
                                //pacote. Cada amostra corresponde a 2 bytes (MSB first).  
#define tCMD_StopAquis  "SPAQ"  //Comando: Parar aquisição de dados e parar envio pela serial para o host
                              

//O loop da aplicação será executado no modelo de máquina de estados
typedef enum
{
  //Estado inicial da máquina de estados - Aguardar handshake Host <-> PIC
  SVIPT_STATE_WAIT_HANDSHAKE_COMPLETED,
  //Aguarda chegada de comando do host
  SVIPT_STATE_WAIT_HOST_COMMAND
  
} SVIPT_STATES;

// Dados da aplicação.
//  This structure holds the application's data.
typedef struct
{
    // Estado atual da máquina de estados
    SVIPT_STATES state;
    //indica se o hanshake Host<->Teensy foi concluído
    bool handshake_ok;   
} SVIPT_DATA;

// Variável para conter os dados da aplicação
SVIPT_DATA sviptData;

volatile bool ledOn; // use volatile for shared variables

//Para aproveitar a banda de transmissão da USB, vou mandar pacotes de bytes a cada transmissão.
//(O pacote USB contém uma série de elementos - se mandarmos, por exemplo, um byte por vez,
// o pacote conterá cerca 64 bytes (Se a Baud rate for, 9600 bps, ocupamos a maior parte dela só
//com overheads do pacote USB).
//MAS, para garantir Real Time no controle da interface que roda no host, preciso garantir
//que os dados cheguem em intervalos não muito longos - no máximo 50ms.
//Como a taxa de amostragem é de 200Hz (T = 5ms) (se tx amostr mudar devo ajustar aqui!!!), em 50ms
//termos digitalizados 10 amostras. Assim, vamos transmitir, em cada pacote, 20 bytes de dados.
//Cada pacote será o seguinte formato:
// | "STAQ" | #bytesPack | bytes pack |
// tCMDSize (atualmente 4) chars com nome do comando, seguido
// de 1 BYTE com a quantidade de bytes do pacote, seguido dos bytes do
//pacote. Cada amostra corresponde a 2 bytes (MSB first).  
#define nBytesDadosUsb 20
byte packUSB[100]; //tamanho, pelo menos = 2*nBytesDadosUsb + tCMDSize + 1 // dados + comando + quant de bytes de dados

//armazena a quantidade de amostras já digitalizadas (para montar o pacote de transmissão USB). 
int nSamplesOk;

// the setup routine runs once when you press reset:
void setup() 
{
  // initialize the LED digital pin as an output.
  pinMode(led, OUTPUT);   
  digitalWrite(led, LOW);
  ledOn = false;

  //Estado inicial da máquina de estados: aguardando finalizaçao de handshake
  sviptData.state = SVIPT_STATE_WAIT_HANDSHAKE_COMPLETED;
  sviptData.handshake_ok = false;

  analogReadResolution(12);
  
  // initialize timer1 
  //  Set timer para aquisição de dados em AD_TxAmost Hertz.
  //  Timer1.initialize(microseconds);
  //Timer1.setFrequency( (int)((1.0/(float)AD_TxAmost) * 1000000.0) );
  
  // initialize serial communication at baudRate bits per second:
  Serial.begin(baudRate);

  //clear serial buffer
  while (Serial.available() > 0) 
 {
    Serial.read();
 }
}

// the loop routine runs over and over again forever:
void loop() 
{
  String StrHost;
  char msgAux[16];
    
  // Executar ações do estado em que a máquina estiver
  switch ( sviptData.state )
  {
        /* Application's initial state. */
        case SVIPT_STATE_WAIT_HANDSHAKE_COMPLETED:
            //Estado inicial da máquina de estados - Aguardar handshake Host <-> Teensy

            //Verifica se o host enviou comando.
            //Comandos STRING terão sempre tCMDSize caracteres SEGUIDOS de caracter um terminador.
            //NÃO PODE SER MUITO MAIOR para evitar overflow de StrHost
            if (Serial.available() >= tCMDSize+1)  //+1 : '\0'
            {               
                // read the incoming byte (readString funciona pois o terminador é '\0':
                StrHost = Serial.readString();
                //É o comando esperado?
                if(StrHost == tCMD_SendFmw)
                {
                  //sim - enviar firmware com terminador
                  //      "FMWRTeensy SVIPT 1.0'\0'"
                  sendUSBmessage(tCMD_SendFmw, firmware, terminatorChar);
                      
                  //mudar para estado a espera de comandos
                  sviptData.state = SVIPT_STATE_WAIT_HOST_COMMAND;
                }
            }
            //Senão: Permanece neste estado...
            break;
      
        case SVIPT_STATE_WAIT_HOST_COMMAND:
            //Aguarda chegada de comando do host

            //Verifica se o host enviou comando.
            //Comandos terão sempre tCMDSize caracteres SEGUIDOS de caracter um terminador.
            //NÃO PODE SER MUITO MAIOR para evitar overflow de StrHost
            if (Serial.available() >= tCMDSize) 
            {                  
                // read the incoming byte (readStrung funciona pois o terminador é '\0':
                StrHost = Serial.readString();
                //É um comando esperado?

                if(StrHost == tCMD_SendFmw)
                {
                  //sim - enviar firmware com terminador
                  //      "FMWRTeensy SVIPT 1.0'\0'"
                  sendUSBmessage(tCMD_SendFmw, firmware, terminatorChar);
                }
                if(StrHost == tCMD_SendADrange)
                {
                    //sim - Host solicita o envio do range do conversor AD.
                  //Teensy envia o nome do comando seguido do range em
                  //um string (range será enviado com uma casa decimal --- 5.0, 3.3 ...)
                  //    "ADRA5.0'\0'"   
                  //converter float to string
                  memset(msgAux,0,16); //limpar possíveis valores anteriores
//                  dtostrf(AD_Range, 3, 1, msgAux);
                  sprintf(msgAux, "%.f",AD_Range);
                  sendUSBmessage(tCMD_SendADrange, msgAux, terminatorChar);           
                }
                if(StrHost == tCMD_SendADnbits)
                {
                  //sim - Host solicita o envio da quantidade de bits do conversor AD.
                  //Os valores digitalizados serao enviados em 2 BYTES para aumentar velocidade de
                  //transmissão. Para isso o host precisa saber: AD_Range e AD_nBits.
                  //Em resposta, Teensy envia o nome do comando seguido do nro de bits em
                  //um string (nbits - integer)  "ADNB10'\0'"
                  //int to char[]
                  memset(msgAux,0,16); //limpar possíveis valores anteriores
                  itoa(AD_nBits,msgAux,10);   //10 - decimal base
                  sendUSBmessage(tCMD_SendADnbits, msgAux, terminatorChar);   
                }
                if(StrHost == tCMD_SendADtxAmost)
                {
                  //Host solicita o envio da taxa de amostragem usada na digitalização.
                  //Os sinais analógicos (da célula de carga serão lidos nesta frequência).
                  //Os valores digitalizados serao enviados em 2 BYTES para aumentar velocidade de
                  //transmissão. Para isso o host precisa saber: AD_Range e AD_nBits.
                  //Em resposta, Teensy envia o nome do comando seguido da tx amostr em
                  //um string (TxAmostr - integer)  "ADNB200'\0'"
                  //int to char[]
                  memset(msgAux,0,16); //limpar possíveis valores anteriores
                  itoa(AD_TxAmost,msgAux,10);   //10 - decimal base
                  sendUSBmessage(tCMD_SendADtxAmost, msgAux, terminatorChar);              
                }               
                if(StrHost == tCMD_StartAquis)
                {
                  //sim - Iniciar coleta de dados e envio para o host via serial
                  //Os valores digitalizados serao enviados em 2 BYTES para aumentar velocidade de
                  //transmissão.
                  //Acende led para indicar coleta de dados
                  digitalWrite(led, HIGH);
                  nSamplesOk = 0; //inicializa contador de amostras para pacote de dados
                  //Timer1.attachInterrupt(getAdData); //callback coleta de dados e envio pela USB
                  Timer3.attachInterrupt(getAdData).setPeriod((int)((1.0/(float)AD_TxAmost) * 1000000.0)).start();
                }
                if(StrHost == tCMD_StopAquis)
                {
                  //Apaga led para indicar fim de coleta
                  digitalWrite(led, LOW);
                  //Parar timer de coleta de dados e envio de dados para o host via serial
                  Timer3.detachInterrupt();
                }
                
            }
            //Permanece neste estado.            
            break;
            
        default:
            break;
  }
  
}

//Enviar uma mensagem para o host no formato:
// | header | msg | terminator |

void sendUSBmessage(char* header, char* msg, char terminator)
{
  int len;
  char msgUSB[100];
  
  strcpy(msgUSB, header);
  strcat(msgUSB, msg);
  len = strlen(header) + strlen(msg);
  msgUSB[len] = terminator;
  Serial.write(msgUSB, len+1); 
}
  

//Função de callback das interrupções do timer para aquisição de dados.
void getAdData(void)
{ 
  //Ler entrada AD da célula de carga
  // read the input on analog pin 0:
  int sensorValue = analogRead(A0); //conversor AD de 10 bits
  //enviar amostra para host em 2 bytes - MSB FIRST
  
  //Para aproveitar a banda de transmissão da USB, vou mandar pacotes de bytes a cada transmissão.
  //(O pacote USB contém uma série de elementos - se mandarmos, por exemplo, um byte por vez,
  // o pacote conterá cerca 64 bytes (Se a Baud rate for, 9600 bps, ocupamos a maior parte dela só
  //com overheads do pacote USB).
  //MAS, para garantir Real Time no controle da interface que roda no host, preciso garantir
  //que os dados cheguem em intervalos não muito longos - no máximo 50ms.

  //Cada pacote será o seguinte formato:
  // | "STAQ" | #bytesPack | bytes pack |
  // tCMDSize (atualmente 4) chars com nome do comando, seguido
  // de 1 BYTE com a quantidade de bytes do pacote, seguido dos bytes do
  //pacote. Cada amostra corresponde a 2 bytes (MSB first).  
  //nBytesDadosUsb 20
  //packUSB[nBytesDadosUsb + tCMDSize + 1]; //+ tCMDSize + 1: para comando e 1 byte com a quant de bytes de dados

  int indexDados = tCMDSize + 1 + (nSamplesOk*2); //posição onde os novos dados serão colocados no pacote
  packUSB[indexDados] = highByte(sensorValue); 
  packUSB[indexDados+1] = lowByte(sensorValue);
  nSamplesOk++;

  //pacote USB completo?
  if(nSamplesOk*2 >= nBytesDadosUsb)
  {
          //Teste
          if(ledOn)
          {
            digitalWrite(led, LOW); ledOn = false;
          }
          else
          {
            digitalWrite(led, HIGH); ledOn = true;
          }

    //completar pacote.
    //Incluir nome do comando no início ("STAQ") 
    for(int i = 0; i < tCMDSize; i++)
    {
      packUSB[i] = tCMD_StartAquis[i]; 
    }
    //quantidade de bytes de dados 
    packUSB[tCMDSize] = 2*nSamplesOk;  

    //transmite o pacote
    Serial.write(packUSB, tCMDSize + 1 + (nSamplesOk*2)); 

    //zera contador de amostras do pacote
    nSamplesOk = 0;
  }
}

